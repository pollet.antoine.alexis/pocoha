import { Component } from '@angular/core';
import { FilmsService } from './service/films.service';
 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(){
  }

}
