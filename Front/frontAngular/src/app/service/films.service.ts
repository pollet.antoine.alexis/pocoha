import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class FilmsService{

    subj = new Subject<any>();

    Listefilms = [
        {
            titre: "Harry Potter",
            genre: "Action",
            annee: "2013",
            img: "assets/Images/HarryPotter.jpg",
            reserve: true
        },
        {
            titre: "FightClub",
            genre: "Action",
            annee: "1999",
            img: "assets/Images/FightClub.jpg",
            reserve: false
        },
        {
            titre: "I Robot",
            genre: "Action",
            annee: "2004",
            img: "assets/Images/IRobot.jpg",
            reserve: false
        },
        {
            titre: "La Ligne Verte",
            genre: "Action",
            annee: "2000",
            img: "assets/Images/LaLigneVerte.jpg",
            reserve: false
        },
        {
            titre: "Les Affranchis",
            genre: "Action",
            annee: "1990",
            img: "assets/Images/LesAffranchis.jpg",
            reserve: true
        },
        {
            titre: "Django",
            genre: "Action",
            annee: "2012",
            img: "assets/Images/Django.jpg",
            reserve: false
        },
        {
            titre: "Les Evades",
            genre: "Action",
            annee: "1994",
            img: "assets/Images/LesEvades.jpg",
            reserve: true
        },
        {
            titre: "Moi moche et méchant 3",
            genre: "Action",
            annee: "2017",
            img: "assets/Images/moiMoche&Mechant.png",
            reserve: false
        },
        {
            titre: "Pulp Fiction",
            genre: "Action",
            annee: "1994",
            img: "assets/Images/PulpFiction.jpg",
            reserve: true
        },
      ]

    emitElements(){
        this.subj.next(this.Listefilms);
    }
    
}