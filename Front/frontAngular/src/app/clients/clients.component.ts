import { Component} from '@angular/core';
import { FilmsComponent } from '../films/films.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent{

  constructor() { }

  clients = [
    {
      id: 20,
      prenom: "Antoine",
      nom: "Pollet",
      mail: "example@gmail.com",
      telephone: "0741526320"
    },
    {
      id: 84,
      prenom: "Maxime",
      nom: "Consigny",
      mail: "example@gmail.com",
      telephone: "0626154859"
    },
    {
      id: 972,
      prenom: "Louis",
      nom: "Hanauer",
      mail: "example@gmail.com",
      telephone: "0750649584"
    },
    {
      id: 12,
      prenom: "Lucas",
      nom: "Juillard",
      mail: "example@gmail.com",
      telephone: "0652148574"
    },
    {
      id: 87,
      prenom: "Ilan",
      nom: "Keller",
      mail: "example@gmail.com",
      telephone: "0652365985"
    },
    {
      id: 62,
      prenom: "Jean",
      nom: "Alfred",
      mail: "example@gmail.com",
      telephone: "0652456895"
    },
    {
      id: 666,
      prenom: "Charle-Henri",
      nom: "Francois",
      mail: "example@gmail.com",
      telephone: "0606060606"
    },
  ]
}
